
public class Principal {

    public static void main(String[] args) {
     
        System.out.println("==> CalculadoraInt --> resultado = "+ Principal.engine((int)6,(int)5).calculate(5,5));
        System.out.println("==> CalculadoraInt --> resultado = "+ Principal.engine((long)8,(long)6).calculate(6,2));
        
    }
    private static CalculadoraInt engine(int a, int b ){
        return (x, y) -> a*b;
    }
    private static CalculadoraLong engine(long a, long b ){
        return (x, y) -> a-b;
    }
}
